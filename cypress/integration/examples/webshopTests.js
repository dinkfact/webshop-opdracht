describe("Testing to see if Cypress works", function() {
  it("Check if empty cart message appears before anything is added.", function() {
    cy.visit("http://localhost:3000/");
    cy.get(".shoppingCart").click();
    cy.get("h3")
      .should("exist")
      .should("have.text", "U heeft nog geen producten in uw winkelwagen.");
  });
});
