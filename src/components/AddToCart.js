import React, { useState } from "react";
import "../index.css";

const AddToCart = props => {
  let [productAmount, setProductAmount] = useState(1);

  const incrementAmount = e => {
    e.preventDefault();
    let newAmount = productAmount + 1;
    setProductAmount(newAmount);
  };

  const decrementAmount = e => {
    e.preventDefault();
    if (productAmount > 0) {
      let newAmount = productAmount - 1;
      setProductAmount(newAmount);
    }
  };

  const onSubmit = e => {
    e.preventDefault();
    props.addToCartList({ product: props.product, amount: productAmount });
  };

  return (
    <div>
      <form onSubmit={onSubmit}>
        <label htmlFor="amount">Aantal: </label>
        <button className="btnMin myButton" onClick={decrementAmount}>
          -
        </button>
        <input
          type="text"
          size="2"
          className="txtProductAmount"
          value={productAmount}
          onChange={e => setProductAmount(e.target.value)}
        />
        <button className="btnPlus myButton" onClick={incrementAmount}>
          +
        </button>
        <button type="submit" className="btnAddToCart myButton">
          Toevoegen aan winkelwagen
        </button>
      </form>
    </div>
  );
};

export default AddToCart;
