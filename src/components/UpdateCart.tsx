import React, { useState, useEffect, ChangeEvent } from "react";
import "../index.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";

const UpdateCart = (props: any) => {
  let [productAmount, setProductAmount] = useState<number>(props.amount);

  const incrementAmount = (e: React.MouseEvent<HTMLElement>): void => {
    e.preventDefault();
    let newAmount = productAmount + 1;
    setProductAmount(newAmount);
  };
  const decrementAmount = (e: React.MouseEvent<HTMLElement>): void => {
    e.preventDefault();
    if (productAmount >= 0) {
      let newAmount = productAmount - 1;
      setProductAmount(newAmount);
    }
  };

  const updateCartList = () => {
    props.updateCartList(props.listIndex, productAmount);
  };

  const removeFromCartList = (e: React.MouseEvent<HTMLElement>): void => {
    e.preventDefault();
    props.removeFromCartList(props.listIndex);
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>): void => {
    setProductAmount(parseFloat(e.target.value));
  };

  useEffect(() => {
    updateCartList();
  });

  return (
    <div>
      <form className="removeFromCartGrid">
        <button className="myButton" onClick={decrementAmount}>
          -
        </button>
        <input
          type="text"
          className="txtProductAmount"
          value={productAmount}
          onChange={handleChange}
        />
        <button className="myButton" onClick={incrementAmount}>
          +
        </button>

        <button
          onClick={removeFromCartList}
          title="verwijder uit winkelwagen"
          className="myButton"
        >
          <FontAwesomeIcon icon={faTrashAlt} />
        </button>
      </form>
    </div>
  );
};

export default UpdateCart;
