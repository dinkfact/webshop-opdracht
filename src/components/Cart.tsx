import React from "react";
import CartProduct from "./CartProduct";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import CartItem from "../types/CartItem";

const Cart = (props: any) => {
  const calcTotal = (items: Array<CartItem>): number => {
    let result = 0;

    items.map((i: CartItem) => {
      result += i.amount * i.product.price;
    });

    return Math.round(result * 100) / 100;
  };

  const switchContent = (): any => {
    if (props.cartList.length !== 0) {
      return (
        <div>
          {props.cartList.map((item: CartItem, index: number) => (
            <CartProduct
              key={index}
              index={index}
              item={item}
              updateCartList={props.updateCartList}
              removeFromCartList={props.removeFromCartList}
            />
          ))}
          <br />
          <h3>Eindtotaal: &euro; {calcTotal(props.cartList)}</h3>
          <br />
          <button onClick={props.checkout} className="myButton">
            Afrekenen
          </button>
        </div>
      );
    } else {
      return (
        <div>
          <br />
          <br />
          <br />
          <h3>U heeft nog geen producten in uw winkelwagen.</h3>
        </div>
      );
    }
  };

  return (
    <div>
      <h2>Mijn Winkelwagen</h2>
      {switchContent()}
      <br />
      <br />
      <br />
      <div className="back" onClick={props.invokeBooleanSetter}>
        <FontAwesomeIcon icon={faArrowRight} />
        {` `}
        Terug naar de webshop
      </div>
      <br />
    </div>
  );
};

export default Cart;
