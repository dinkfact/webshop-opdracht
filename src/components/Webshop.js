import React, { useState } from "react";
import ProductList from "./ProductList";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import Cart from "./Cart";

const Webshop = props => {
  const [productList] = useState([
    {
      title: "Asus ROG Strix GL504GM-ES070T-BE",
      price: 1199,
      description: `Deze gaming laptop van ASUS is voorzien van een 120Hz beeldscherm.
     Door de combinatie met de snelle videokaart en dit beeldscherm zullen al je games 
     een hoge kwaliteit kennen en beelden vloeiend over je scherm rollen. Zoek je de beste 
     van de beste gaming laptops om je concurrentie te verslaan? Met deze laptop speel je 
     vrijwel alle games zoals GTA V, Civilization, PUBG en Battlefield op hoge instellingen 
     en speel je jezelf fluitend naar de overwinnig. De krachtige Intel Core i7 processor zorgt 
     ervoor dat taken snel worden uitgevoerd, waardoor de laptop geschikt is voor het werken 
     met zwaardere toepassingen. Deze processor biedt de beste prestaties voor het spelen van 
     games en foto- en videobewerking. Door de combinatie van de SSD- en harde schijf start 
     je laptop snel op en beschik je over een ruime opslagcapaciteit. Dankzij 16 GB werkgeheugen 
     kan deze laptop meerdere taken gelijktijdig uitvoeren. Met de NVIDIA GeForce GTX 1060 kun je 
     de meest recente games op hoge grafische beeldinstellingen spelen, waardoor de kleinste details 
     scherp worden weergegeven.`
    },
    {
      title: "Bose SoundTouch 10",
      price: 184.99,
      description: `Met de Bose SoundTouch 10 gaat er een wereld aan muziek voor je open.
       Deze compacte wifi speaker komt het best tot zijn recht in kleine ruimtes.
        Plaats de speaker in je woning, sluit hem aan op netstroom en krijg toegang tot miljoenen 
        nummers van Spotify en Deezer, internetradio en je eigen muziekbibliotheek. 
        Dankzij Spotify Connect staat de luidspreker direct in contact met de muziekdatabase van deze streamingdienst. 
        Zo verbruik je geen kostbare mobiele data om te streamen en selecteer je nummers direct in de Spotify app. 
        Voor muziek van andere diensten of je eigen collectie gebruik je de Bose app. Apparaten zonder wifi 
        of Bluetooth sluit je via de aux aansluiting aan op de luidspreker.`
    },
    {
      title: "Logitech M330 Silent Draadloze Muis",
      price: 29.99,
      description: `Laat je nooit meer afleiden door klikgeluiden met de Logitech M330 Silent Draadloze Muis.
       Deze muis heeft namelijk stille knoppen met rubberen switches. 
       Hierdoor wordt het geluid gedempt en maakt een klik 90% minder geluid dan standaard muizen terwijl 
       het vertrouwde klikgevoel bewaard blijft. Sluit de usb nano ontvanger aan op je laptop of pc en 
       je maakt direct draadloos verbinding met de muis. Constant batterijen verwisselen is verleden tijd, 
       want de muis houdt het tot 24 maanden lang vol op een enkele batterij. Gebruik je de muis even niet? 
       Je hoeft de muis niet uit te zetten, want de M330 schakelt zelf over naar de slaapstand tot je hem weer gebruikt.`
    },
    {
      title: "Acer Nitro VG220Qbmiix",
      price: 119,
      description: `De Acer Nitro VG220Qbmiix is geschikt voor het spelen van games dankzij de snelle reactietijd. 
      Het Full HD scherm biedt een hoge beeldkwaliteit en scherpe weergave. 
      Het IPS-paneel heeft in vergelijking met een TN-paneel twee grote voordelen: een betere kijkhoek en een nauwkeurige kleurweergave. 
      Deze 21,5 inch monitor is met een beelddiagonaal van 53 cm kleiner dan een gemiddelde monitor van 24 inch (61 cm). 
      De Acer Nitro VG220Qbmiix heeft daarnaast een erg strak design dankzij het ZeroFrame ontwerp. De schermranden zijn nauwelijks te zien.

      Dankzij de snelle reactietijd van 1 ms en hoge verversingssnelheid heb je geen last van motion blur en ghosting. 
      Ideaal voor de weergave bij snelle bewegingen, zoals bij shooters of race games. Daarnaast heeft deze monitor 
      een verversingssnelheid van 75 Hz wat zorgt voor vlotte beelden tijdens het gamen. Je kunt deze monitor alleen kantelen, 
      niet in hoogte verstellen of draaien. Dit model beschikt ook over ingebouwde speakers. 
      
      Door Flicker Free technologie en het blauw licht filter is het fijner om voor een langere tijd achter de monitor 
      te zitten en heb je minder snel last van geïrriteerde ogen. De monitor beschikt over ingebouwde speakers en een HDMI-aansluiting. 
      Handig voor als je de monitor wilt aansluiten aan je PlayStation of Xbox. Dit model ondersteunt bovendien FreeSync, 
      wat ervoor zorgt dat de verversingssnelheid en het aantal frames per seconde (FPS) synchroon lopen. 
      Dit zorgt er voor dat het beeld vloeiender loopt, zonder dat je last krijgt dat het beeld verspringt (ook wel tearing genoemd). 
      FreeSync werkt alleen met videokaarten van AMD.`
    },
    {
      title: "Kensington UH4000 4 poorts USB 3.0 Hub",
      price: 27.99,
      description: `Met de Kensington UH4000 4 poorts USB 3.0 Hub breid je de usb a poort van je laptop of desktop uit met 4 usb a 3.0 poorten. 
      Zo heb je altijd genoeg poorten om jouw randapparatuur op aan te sluiten.`
    },
    {
      title: "JBL T460BT - Draadloze on-ear koptelefoon",
      price: 33,
      description: `De JBL T460BT is een prima koptelefoon voor elke dag, waar en wanneer dan ook. 
      Dankzij het lichte gewicht en opvouwbare design neem je deze koptelefoon makkelijk overal mee naartoe. 
      Je dagelijkse ritje naar school of werk wordt een feest met krachtig JBL Pure Bass geluid op je oorschelpen. 
      De feest kan tot wel 11 uur duren, want zo lang gaat de accu namelijk mee. 
      Verder beschikt de JBL T460BT over een ingebouwde microfoon, waardoor je gemakkelijk gesprekken 
      kunt aannemen zonder je smartphone uit je zak te halen. Ideaal voor ieder moment!`
    },
    {
      title: "MSI MEG X399 CREATION moederbord",
      price: 499,
      description: `De MSI MEG X399 Creation is een Extended ATX Socket TR4 moederbord gebaseerd op de AMD X399 chipset, 
      geschikt voor AMD Ryzen Threadripper processors. Het bord dient gecombineerd te worden met DDR4 geheugen, 
      waarvoor 8 sloten beschikbaar zijn. Dankzij 3-way SLI ondersteuning kunnen tot drie videokaarten gecombineerd worden. 
      In totaal heeft de MSI MEG X399 Creation 8 Serial ATA aansluitingen voor harddisks en SSD's. 
      Voor moderne PCI-Express SSD's is het bord voorzien van een snel M.2-slot met PCI-Express 3.0 x4 interface. 
      De onboard geluidskaart biedt 7.1 kanaals geluid en is gebaseerd op een Realtek ALC1220 chip. Het bord biedt 2 netwerkaansluitingen, 
      waarbij je maximaal met 1000 Mbps kunt verbinden. Draadloos verbinden met een netwerk is mogelijk dankzij een geïntegreerde 802.11ac adapter.`
    },
    {
      title: "Kingston ValueRAM - Geheugen",
      price: 679,
      description: `Deze geheugenmodule heeft een capaciteit van 64 GB (één module van 64 GB) en 
      is geschikt voor systemen met een DDR4 geheugenbus van 2400 MHz.`
    },
    {
      title: "MSI GeForce GTX 1050 TI GAMING 4G",
      price: 161.05,
      description: `De GeForce GTX 1050 Ti Gaming 4G van MSI is gebaseerd op de NVIDIA GeForce GTX 1050 Ti Chip en beschikt 
      over 4096 MB GDDR5 Geheugen dat via een 128 bit brede interface aangesproken wordt. De GPU heeft een kloksnelheid van 1316 MHz 
      en het geheugen een snelheid van 7008 MHz.
      Met de GPU Boost 3.0 wordt de kloksnelheid van de GeForce GTX 1050 Ti Gaming 4G geboost tot 1430 MHz tijdens zware grafische applicaties.`
    },
    {
      title: "Acer Predator XB241Hbmipr - LED-monitor",
      price: 349,
      description: `De Predator XB1 van Acer is een 24" LED Monitor met een kijkhoek van 178°, 
      een dynamisch contrast van 100000000:1 en een helderheid van 350 cd/m². 
      Met een resolutie van 1920x1080 pixels, een reactietijd van 1 ms (grijs naar grijs) en geïntegreerde 
      speakers is deze monitor geschikt voor elke multimediatoepassing. De monitor beschikt over een HDMI en 
      DisplayPort aansluiting en is ideaal voor thuis- of kantoorgebruik.`
    }
  ]);

  const [cartList, setCartList] = useState([]);

  const [cartAmount, setcartAmount] = useState(0);

  const [isCartShowing, setIsCartShowing] = useState(false);

  const addToCartList = item => {
    cartList.push(item);
    setCartList(cartList);
    alert(
      `${item.amount} x ${
        item.product.title
      } werd(en) toegevoegd aan uw winkelwagen!`
    );
    setcartAmount(cartList.length);
  };

  // Replaces an array element at the given index by the given value
  const replaceAt = (array, index, value) => {
    const ret = array.slice(0);
    ret[index] = value;
    return ret;
  };

  const updateCartList = (index, newAmount) => {
    let productToUpdate = cartList[index];
    productToUpdate.amount = newAmount;
    let newCartList = replaceAt(cartList, index, productToUpdate);
    setCartList(newCartList);
  };

  const removeFromCartList = index => {
    /*
    let copy = cartList;
    console.log(copy);
    copy.splice(index, 1);
    setCartList(copy);
    console.log(cartList);
    */

    let res = [];

    for (let i = 0; i < cartList.length; i++) {
      if (i !== index) {
        res.push(cartList[i]);
      }
    }

    setCartList(res);
    setcartAmount(cartList.length - 1);
  };

  const checkout = () => {
    let emptyCartList = [];
    setCartList(emptyCartList);
    setcartAmount(0);
    alert("De betaling is afgerond, bedankt om bij ons te winkelen!");
  };

  const invokeBooleanSetter = () => {
    setIsCartShowing(!isCartShowing);
  };

  const switchContent = isCartShowing => {
    if (isCartShowing) {
      return (
        <div>
          <br />
          <Cart
            cartList={cartList}
            invokeBooleanSetter={invokeBooleanSetter}
            updateCartList={updateCartList}
            removeFromCartList={removeFromCartList}
            checkout={checkout}
          />
        </div>
      );
    } else {
      return (
        <div>
          <div className="shoppingCart" onClick={invokeBooleanSetter}>
            Mijn winkelwagen
            <FontAwesomeIcon icon={faShoppingCart} />
            <div className="cartAmount">{cartAmount}</div>
          </div>
          <br />
          <ProductList
            productList={productList}
            addToCartList={addToCartList}
          />
        </div>
      );
    }
  };

  return switchContent(isCartShowing);
};

export default Webshop;
