import React from "react";
import UpdateCart from "./UpdateCart";
import CartItem from "../types/CartItem";

const CartProduct = (props: any) => {
  const calcSum = (item: CartItem): number => {
    let result = item.amount * item.product.price;

    return Math.round(result * 100) / 100;
  };

  return (
    <div>
      <div className="cartProductDiv">
        <div className="cartProductGrid">
          <h3>Product</h3>
          <h3>Aantal</h3>
          <h3>Eenheidsprijs</h3>
          <h3>Totaal</h3>

          <div>{props.item.product.title}</div>
          <UpdateCart
            amount={props.item.amount}
            updateCartList={props.updateCartList}
            removeFromCartList={props.removeFromCartList}
            listIndex={props.index}
          />
          <div>{`€ ${props.item.product.price}`}</div>
          <div id="sumDiv">{`€ ${calcSum(props.item)}`}</div>
        </div>
      </div>
    </div>
  );
};

export default CartProduct;
