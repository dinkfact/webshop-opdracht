import React from "react";
import Product from "./Product";
import "../index.css";

const ProductList = props => {
  return (
    <div className="productListGrid">
      {props.productList.map((product, index) => (
        <Product
          key={index}
          product={product}
          addToCartList={props.addToCartList}
        />
      ))}
    </div>
  );
};

export default ProductList;
