import React from "react";
import AddToCart from "./AddToCart";

const Product = props => {
  return (
    <div className="productDiv">
      <h2>{props.product.title}</h2>
      <p>&euro; {props.product.price}</p>
      <p>{props.product.description}</p>
      <AddToCart product={props.product} addToCartList={props.addToCartList} />
    </div>
  );
};

export default Product;
