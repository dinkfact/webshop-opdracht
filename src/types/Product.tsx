type Product = { title: string; price: number; description: string };

export default Product;
