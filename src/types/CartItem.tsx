import Product from "./Product";

type CartItem = { product: Product, amount: number };

export default CartItem;
