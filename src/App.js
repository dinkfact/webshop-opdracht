import React from "react";
import "./App.css";
import Webshop from "./components/Webshop";

const App = props => {
  return (
    <div className="App">
      <h1>Verhaeghe IT Parts</h1>
      <Webshop />
    </div>
  );
};

export default App;
